\input{include/header.tex}
\input{include/theories/ipl.tex}

\title{Assignment 1}
\subtitle{Basic terminology and unification}

% reset to default spacing between lists (overriding header setting)
\setlist[enumerate]{}


\begin{document}

\maketitle

In this assignment we will be working with the same rules of inference from the notes titled \insist{2: A fragment of propositional logic}. This includes the special \assumed\ axiom and these rules:

\begin{center}
  \TopIntro

  \AndIntro \quad \AndElimL \quad \AndElimR

  \OrIntroL \quad \OrIntroR

  \ImplElim
\end{center}

\begin{enumerate}
  \item \textbf{Terminology} (2 points each)
    \begin{enumerate}
      \item Intuitively, what is the difference between a proposition and a proof? Respond in one or two sentences.
      \item Give an example of a sentence which is not a proposition (or would usually not be considered a proposition), and explain why not. You must give a sentence that is \insist{not} listed as an example in the lecture notes titled ``0: Introduction''.
    \end{enumerate}

  \item \textbf{Substitution} (2 points each)

    For each of the following definitions of $e$ (an expression) and $\sigma$ (a substitution), give $e[\sigma]$.
    \begin{enumerate}
      \item
        Operators: $\position{\rtimes}\position$, $\position{\ltimes}\position$, \mono{P}, \mono{Q}

        $e \defequals \metavar{a} \ltimes \left(\metavar{b} \rtimes \metavar{a}\right)$

        $\sigma \defequals \begin{cases}
          \metavar{a} & \mapsto\ \mono{Q} \\
          \metavar{b} & \mapsto\ \mono{P} \\
          \metavar{c} & \mapsto\ \mono{Q}
        \end{cases}$
      \item
        Operators: $-\position$, $\position{+}\position$, $\position{*}\position$, $\mono{zero}$, $\mono{one}$

        $e \defequals \left(- \left(\metavar{x} * \left(- \metavar{y}\right)\right)\right) + \left(\left(- \metavar{z}\right) + \metavar{x}\right)$

        $\sigma \defequals \begin{cases}
          \metavar{z} & \mapsto\ \mono{zero} \\
          \metavar{y} & \mapsto\ \mono{one} \\
          \metavar{x} & \mapsto\ \mono{one} * \mono{zero}
        \end{cases}$
    \end{enumerate}

  \item \textbf{Unification} (3 points each)

    For each of the following unification problems, give a unifying substitution or write ``no substitution'' if the problem has no unifying substitution. You don't have to use the algorithm from the notes, but you'll get partial credit for showing your work even if you end up with the wrong answer.
    \begin{enumerate}
      \item
        Operators: $\position{+}\position$, \mono{zero}

        $\left\{\begin{array}{r c l}
          \mono{zero} & \unifyequals & \mono{zero} + \mono{zero}
        \end{array}\right\}$
      \item
        Operators: $\position{\rtimes}\position$, $\position{\ltimes}\position$, \mono{P}, \mono{Q}

        $\left\{\begin{array}{r c l}
          \metavar{a} \ltimes \left(\metavar{b} \rtimes \metavar{a}\right) & \unifyequals & \left(\mono{P} \rtimes \mono{P}\right) \ltimes \left(\left(\mono{Q} \ltimes \mono{Q}\right) \rtimes \left(\mono{P} \rtimes \mono{P}\right)\right)
        \end{array}\right\}$
      \item
        Operators: $\position{\rtimes}\position$, $\position{\ltimes}\position$, \mono{P}, \mono{Q}

        $\left\{\begin{array}{r c l}
          \metavar{a} \ltimes \metavar{b} & \unifyequals & \left(\mono{P} \rtimes \mono{Q}\right) \ltimes \left(\left(\mono{P} \rtimes \mono{Q}\right) \ltimes \left(\mono{P} \rtimes \mono{Q}\right)\right) \\
          \metavar{b} \rtimes \metavar{a} & \unifyequals & \left(\left(\mono{P} \rtimes \mono{Q}\right) \ltimes \left(\mono{P} \rtimes \mono{Q}\right)\right) \rtimes \left(\mono{P} \rtimes \mono{Q}\right)
        \end{array}\right\}$
      \item
        Operators: $\position{\rtimes}\position$, $\position{\ltimes}\position$, \mono{P}, \mono{Q}

        $\left\{\begin{array}{r c l}
          \metavar{a} \ltimes \metavar{b} & \unifyequals & \left(\mono{P} \rtimes \mono{Q}\right) \ltimes \left(\left(\mono{P} \rtimes \mono{Q}\right) \rtimes \left(\mono{P} \rtimes \mono{Q}\right)\right) \\
          \metavar{b} \rtimes \metavar{a} & \unifyequals & \left(\left(\mono{P} \rtimes \mono{Q}\right) \rtimes \left(\mono{P} \rtimes \mono{Q}\right)\right) \ltimes \left(\mono{P} \rtimes \mono{Q}\right)
        \end{array}\right\}$
      \item
        Operators: $-\position$, $\position{+}\position$, $\position{*}\position$, $\mono{zero}$, $\mono{one}$

        $\left\{\begin{array}{r c l}
          \left(- \left(\metavar{x} * \left(- \metavar{y}\right)\right)\right) + \left(\left(- \metavar{z}\right) + \metavar{x}\right) & \unifyequals & \left(- \left(\mono{zero} * \left(- \mono{one}\right)\right)\right) + \left(\left(- \mono{zero}\right) + \mono{one}\right) 
        \end{array}\right\}$
      \item
        Operators: $-\position$, $\position{+}\position$, $\position{*}\position$, $\mono{zero}$, $\mono{one}$

        $\left\{\begin{array}{r c l}
          - \left(\metavar{x} * \left(- \metavar{y}\right)\right) & \unifyequals & - \left(\left(\mono{one} + \mono{one}\right) * \left(- \left(- \mono{zero}\right)\right)\right) \\
          \left(- \metavar{z}\right) + \metavar{x} & \unifyequals & \left(- \mono{zero}\right) + \left(\mono{one} + \mono{one}\right)
        \end{array}\right\}$
    \end{enumerate}

  \item \textbf{Proofs on paper} (4 points each)

    Construct a complete proof tree for each of the following problem statements. (There may be more than one possible proof for some; you just have to give one correct proof for each.)
    \begin{enumerate}
      \item
        \AxiomC{\assumption{$\mono{P}$}}
        \UnaryInfC{$\left(\mono{P} \vee \mono{Q}\right) \wedge \left(\mono{P} \vee \mono{R}\right)$}
        \DisplayProof
      \item
        \AxiomC{\assumption{$\mono{P}$}}
        \AxiomC{\assumption{$\mono{P} \subset \left(\mono{Q} \wedge \mono{R}\right)$}}
        \BinaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \vee \left(\mono{P} \wedge \mono{R}\right)$}
        \DisplayProof
      \item
        \AxiomC{\assumption{$\mono{R}$}}
        \AxiomC{\assumption{$\left(\mono{S} \vee \mono{R}\right) \subset \left(\mono{P} \wedge \mono{Q}\right)$}}
        \BinaryInfC{$\mono{Q} \wedge \mono{P}$}
        \DisplayProof
      \item
        \AxiomC{\assumption{$\mono{P}$}}
        \AxiomC{\assumption{$\mono{Q}$}}
        \AxiomC{\assumption{$\left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R}$}}
        \TrinaryInfC{$\mono{R}$}
        \DisplayProof
    \end{enumerate}

\end{enumerate}

\end{document}
