-- Each exercise is worth five points, for a total of 40.

-- Solve each of the exercises in the code below. You can get partial
-- credit for writing out your thought process in comments even if you
-- don't have a final answer that Agda accepts.

-- You can modify each exercise definition to add new arguments and
-- pattern-match on arguments. Don't change the type signature of an
-- exercise (the line with the : operator) because then it's not the
-- same exercise anymore!

-- None of these exercises require introducing additional lemmas
-- beyond what's already provided, but you're allowed to introduce
-- lemmas if you want.

-- As usual, please don't hesitate to reach out for help!

{-# OPTIONS --type-in-type #-}

data ⊤ : Set where
  tt : ⊤

data ⊥ : Set where

⊥-elim : ∀ {A : Set} → ⊥ → A
⊥-elim ()

¬_ : Set → Set
¬_ A = A → ⊥

¬-elim : ∀ {A B : Set} → ¬ A → A → B
¬-elim na a = ⊥-elim (na a)

data _∧_ : Set → Set → Set where
  _,_ : ∀ {A B : Set} → A → B → A ∧ B

data _∨_ : Set → Set → Set where
  left : ∀ {A B : Set} → A → A ∨ B
  right : ∀ {A B : Set} → B → A ∨ B

data _≡_ : ∀ {A : Set} → A → A → Set where
  refl : ∀ {A : Set} (x : A) → x ≡ x

sym : ∀ {A : Set} {x y : A} → x ≡ y → y ≡ x
sym (refl x) = refl x

trans : ∀ {A : Set} {x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans (refl x) (refl x) = refl x

cong : ∀ {A B : Set} (f : A → B) {x y : A} → x ≡ y → f x ≡ f y
cong f (refl x) = refl (f x)

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

one : ℕ
one = suc zero

_+_ : ℕ → ℕ → ℕ
zero + n = n
(suc m) + n = suc (m + n)

_*_ : ℕ → ℕ → ℕ
zero * n = zero
(suc m) * n = n + (m * n)

-- exponentiation
_^_ : ℕ → ℕ → ℕ
m ^ zero = one
m ^ (suc n) = m * (m ^ n)

+-zeroR : ∀ (n : ℕ) → (n + zero) ≡ n
+-zeroR zero = refl zero
+-zeroR (suc n) = cong suc (+-zeroR n)

exercise1 : ∀ {A : Set} → (⊤ → A) → A
exercise1 = {!!}

exercise2 : ∀ {A B C : Set} → (A ∨ B) ∧ C → (A ∧ C) ∨ (B ∧ C)
exercise2 = {!!}

exercise3 : ∀ {A B C : Set} → (A → (B → C)) → ((A ∧ B) → C)
exercise3 = {!!}

exercise4 : ∀ {A B : Set} → (A → B) → (¬ B → ¬ A)
exercise4 = {!!}

exercise5 : ∀ {A B : Set} → ((¬ A) ∨ B) → (A → B)
exercise5 = {!!}

exercise6 : ∀ {A : Set} → ¬ ¬ ((¬ A) ∨ A)
exercise6 = {!!}

exercise7 : ∀ (n : ℕ) → ((zero ^ n) ≡ zero) ∨ ((zero ^ n) ≡ one)
exercise7 = {!!}

exercise8 : ∀ (n : ℕ) → (one ^ n) ≡ one
exercise8 = {!!}
