# 1. build type of propositions
# 2. build unification function for equations between propositions
# 3. build type of proof trees
# 4. build some representation of inference rules
# 5. build checking function for proof trees

from __future__ import annotations
from dataclasses import dataclass

@dataclass
class Meta:
    name: str

    def __str__(self):
        return f"[{self.name}]"

@dataclass
class Top:
    def __str__(self):
        return "⊤"

@dataclass
class Or:
    left: Prop
    right: Prop

    def __str__(self):
        return f"({self.left} ∨ {self.right})"

@dataclass
class And:
    left: Prop
    right: Prop

    def __str__(self):
        return f"({self.left} ∧ {self.right})"

@dataclass
class Impl:
    left: Prop
    right: Prop

    def __str__(self):
        return f"({self.left} → {self.right})"

Prop = Meta | Top | Or | And | Impl

def count_nodes(prop: Prop) -> int:
    match prop:
        case Meta(_): return 1
        case Top(): return 1
        case Or(left, right): return count_nodes(left) + count_nodes(right)
        case And(left, right): return count_nodes(left) + count_nodes(right)
        case Impl(left, right): return count_nodes(left) + count_nodes(right)

def is_closed(prop: Prop) -> bool:
    match prop:
        case Meta(_): return False
        case Top(): return True
        case Or(left, right): return is_closed(left) and is_closed(right)
        case And(left, right): return is_closed(left) and is_closed(right)
        case Impl(left, right): return is_closed(left) and is_closed(right)

def subst(sigma: dict[str, Prop], prop: Prop) -> Prop:
    match prop:
        case Meta(x): return sigma[x] if x in sigma else Meta(x)
        case Top(): return Top()
        case Or(left, right): return Or(subst(sigma, left), subst(sigma, right))
        case And(left, right): return And(subst(sigma, left), subst(sigma, right))
        case Impl(left, right): return Impl(subst(sigma, left), subst(sigma, right))

def unify(prop1: Prop, prop2: Prop) -> None | dict[str, Prop]:
    assert is_closed(prop2)
    match (prop1, prop2):
        case (Meta(name), _): return { name: prop2 }
        case (Top(), Top()): return {}
        case (Or(l1, r1), Or(l2, r2)): return unify2(l1, l2, r1, r2)
        case (And(l1, r1), And(l2, r2)): return unify2(l1, l2, r1, r2)
        case (Impl(l1, r1), Impl(l2, r2)): return unify2(l1, l2, r1, r2)

def unify2(l1: Prop, l2: Prop, r1: Prop, r2: Prop) -> None | dict[str, Prop]:
    left_sub = unify(l1, l2)
    right_sub = unify(r1, r2)
    if left_sub is None or right_sub is None:
        return None
    else:
        result = {}
        for meta, prop in left_sub.items():
            if meta not in right_sub:
                result[meta] = prop
            elif right_sub[meta] != prop:
                return None
        for meta, prop in right_sub.items():
            result[meta] = prop
        return result
