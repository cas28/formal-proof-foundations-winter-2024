from __future__ import annotations
from dataclasses import dataclass
from typing import ClassVar

from prop import *

# T-Intro
# ∧-Intro
# ∧-ElimL/R
# ∨-IntroL/R
# →-Elim

@dataclass
class TopIntro:
    conclusion: Prop
    rule_conclusion: ClassVar[Prop] = Top()

@dataclass
class AndIntro:
    left: Proof
    right: Proof
    conclusion: Prop

    rule_premise_left: ClassVar[Prop] = Meta("a")
    rule_premise_right: ClassVar[Prop] = Meta("b")
    rule_conclusion: ClassVar[Prop] = And(Meta("a"), Meta("b"))

@dataclass
class AndElimL:
    child: Proof
    conclusion: Prop

    rule_premise: ClassVar[Prop] = And(Meta("a"), Meta("b"))
    rule_conclusion: ClassVar[Prop] = Meta("a")

@dataclass
class AndElimR:
    child: Proof
    conclusion: Prop

    rule_premise: ClassVar[Prop] = And(Meta("a"), Meta("b"))
    rule_conclusion: ClassVar[Prop] = Meta("b")

@dataclass
class OrIntroL:
    child: Proof
    conclusion: Prop

    rule_premise: ClassVar[Prop] = Meta("a")
    rule_conclusion: ClassVar[Prop] = Or(Meta("a"), Meta("b"))

@dataclass
class OrIntroR:
    child: Proof
    conclusion: Prop

    rule_premise: ClassVar[Prop] = Meta("b")
    rule_conclusion: ClassVar[Prop] = Or(Meta("a"), Meta("b"))

@dataclass
class ImplElim:
    left: Proof
    right: Proof
    conclusion: Prop

    rule_premise_left: ClassVar[Prop] = Impl(Meta("a"), Meta("b"))
    rule_premise_right: ClassVar[Prop] = Meta("a")
    rule_conclusion: ClassVar[Prop] = Meta("b")

Proof = (
    TopIntro |
    AndIntro | AndElimL | AndElimR |
    OrIntroL | OrIntroR |
    ImplElim
)

def check(proof: Proof) -> bool:
    match proof:
        case TopIntro(conclusion):
            assert is_closed(conclusion)
            return unify(TopIntro.rule_conclusion, conclusion) is not None

        case AndIntro(left, right, conclusion):
            assert is_closed(conclusion)
            sub = unify(AndIntro.rule_conclusion, conclusion)
            if sub is None: return False
            premise_left = subst(sub, AndIntro.rule_premise_left)
            premise_right = subst(sub, AndIntro.rule_premise_right)
            sub_left = unify(premise_left, left.conclusion)
            sub_right = unify(premise_right, right.conclusion)
            assert is_closed(left.conclusion)
            assert is_closed(right.conclusion)
            if sub_left is None: return False
            if sub_right is None: return False
            return check(left) and check(right)

        case AndElimL(child, conclusion):
            assert is_closed(conclusion)
            sub = unify(AndElimL.rule_conclusion, conclusion)
            if sub is None: return False
            premise = subst(sub, AndElimL.rule_premise)
            sub_premise = unify(premise, child.conclusion)
            assert is_closed(child.conclusion)
            if sub_premise is None: return False
            return check(child)

        case AndElimR(child, conclusion):
            assert is_closed(conclusion)
            sub = unify(AndElimR.rule_conclusion, conclusion)
            if sub is None: return False
            premise = subst(sub, AndElimR.rule_premise)
            sub_premise = unify(premise, child.conclusion)
            assert is_closed(child.conclusion)
            if sub_premise is None: return False
            return check(child)

        case OrIntroL(child, conclusion):
            assert is_closed(conclusion)
            sub = unify(OrIntroL.rule_conclusion, conclusion)
            if sub is None: return False
            premise = subst(sub, OrIntroL.rule_premise)
            sub_premise = unify(premise, child.conclusion)
            assert is_closed(child.conclusion)
            if sub_premise is None: return False
            return check(child)

        case OrIntroR(child, conclusion):
            assert is_closed(conclusion)
            sub = unify(OrIntroR.rule_conclusion, conclusion)
            if sub is None: return False
            premise = subst(sub, OrIntroR.rule_premise)
            sub_premise = unify(premise, child.conclusion)
            assert is_closed(child.conclusion)
            if sub_premise is None: return False
            return check(child)

        case ImplElim(left, right, conclusion):
            assert is_closed(conclusion)
            sub = unify(ImplElim.rule_conclusion, conclusion)
            if sub is None: return False
            premise_left = subst(sub, ImplElim.rule_premise_left)
            premise_right = subst(sub, ImplElim.rule_premise_right)
            sub_left = unify(premise_left, left.conclusion)
            sub_right = unify(premise_right, right.conclusion)
            assert is_closed(left.conclusion)
            assert is_closed(right.conclusion)
            if sub_left is None: return False
            if sub_right is None: return False
            return check(left) and check(right)

def search(prop: Prop) -> Proof | None:
    pass

if __name__ == "__main__":
    prop: Prop = Or(
        And(Top(), Top()),
        Or(Top(), Top())
    )

    proof: Proof = OrIntroR(
        OrIntroL(
            TopIntro(Top()),
            Or(Top(), Top())
        ),
        prop
    )

    print(check(proof))
