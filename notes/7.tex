\input{include/header.tex}

% auto-generated glossary terms
\input{notes/7.glt}

% rules of inference
\input{include/theories/iplc.tex}


\indextitle{Proofs in the positive fragment of IPL}
\subtitle{\ldots and a little taste of type theory}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}


  With our complete definition of the \vocab{positive-fragment} of \vocabstyle{\acrlong{ipl}}, there are several notable properties that we can prove about our basic propositional operators (\vocab{sym-wedge}, \vocab{sym-vee}, and \vocab{sym-subset}). Before we move on to introduce \vocab{negation}, we'll take a little time to introduce some of these properties and discuss their significance.

  This will also be a good opportunity to check back in with Agda for some help working with \vocabs{context} and with our new \implIntro\ and \orElim\ rules. In our Agda definition of the full \vocab{positive-fragment} of \acrshort{ipl}, we'll see how a tiny bit of \vocab{type-theory} can help us structure the rules of our proof system more carefully.

  In our previous Agda adventure, we used Agda's support for \vocabs{variable} to manage our assumptions. In the Agda code for these notes, we'll be managing our context of assumptions in a different and somewhat painfully explicit way, without taking much advantage of Agda's own support for managing assumptions.

  This is just for the sake of exercise, to make sure that the theory of \acrshort{ipl} is presented as clearly and unambiguously as possible. We'll see soon how Agda's variables relate to our theory of contexts, which will allow us to use Agda's support for variables along with our upgraded rules.



\section{Rules}


  As a reminder, here are all of our \vocabs{rules-of-inference} and \vocabs{derived-rule} that define \acrshort{ipl}.


  \subsection{Inference rules}

    \begin{center}
      \Here \quad \There \quad \Assumed

      \SubNil \quad \SubCons

      \TopIntro

      \AndIntro \quad \AndElimL \quad \AndElimR

      \OrIntroL \quad \OrIntroR

      \OrElim

      \ImplIntro \quad \ImplElim
    \end{center}


  \subsection{Derived rules}

    \begin{center}
      \SubRefl \quad \SubTrans

      \Rearrange \quad \Satisfy
    \end{center}

    We don't have fully formal definitions of the \inAt\ and \subAt\ rules, but they are also valid derived rules that we can use in \acrshort{ipl} proof trees. Review the notes on \vocabs{context} and \vocabs{structural-rule} for their informal definitions.


  \subsection{In Agda}

    Like before, we start our module with a module declaration, but this time we'll be defining our operators and rules of inference in a slightly different way.

    \LongAgdaSnippet{module}

    \subsubsection{Fixity}

      Remember that there are some operators which are always at the root of the syntax tree whenever they appear:

      \begin{itemize}
        \item $\position{\in}\position$
        \item $\position{\subsume}\position$
        \item $\position{\entails}\position$
      \end{itemize}

      We can represent this property in Agda with a \vocab{fixity} declaration for these operators. Fixity declarations are a relatively minor detail of our code, and they do not affect the logical content of our proofs; they just make some of our \vocabs{expression} easier to write.

      The \AgdaNumber{0} in \AgdaKeyword{infix} \AgdaNumber{0} indicates that these operators will always come last in the ``order of operations''.

      \LongAgdaSnippet{infix0}

      We also discussed in previous notes how the $\position{\cons}\position$ operator always ``leans to the right'' in its syntax trees. In this Agda code, we will have an additional comma operator (written $\position{,}\position$) which will also ``lean to the right''.

      The \AgdaKeyword{infixr} \AgdaNumber{1} declaration indicates that these operators will come \insist{before} the ones defined with \AgdaKeyword{infix} \AgdaNumber{0} in the ``order of operations'': the higher number indicates higher \vocab{precedence}, or higher ``priority''. The \AgdaKeyword{r} in \AgdaKeyword{infixr} indicates that the syntax trees for these operators will also ``lean to the right''.

      \LongAgdaSnippet{infixr1}

      This is why we're defining our operators and rules in a different way than last time: operators defined in a module \vocab{telescope} don't support fixity declarations (in stable versions of Agda at time of writing).

    \subsubsection{Types}

      We can use the \AgdaKeyword{postulate} keyword to define operators and rules in a slightly different way. The \AgdaKeyword{postulate} keyword applies to everything indented below it. The definitions we define in the indented block are available in the rest of the module.

      The first definitions we'll give are for two different \vocabs{type}, in order to keep track of the difference between \vocabs{proposition} and \vocabs{context}.

      In the last Agda that we looked at, we represented all of our \vocabs{expression} with the built-in type \AgdaPrimitive{Set}. \AgdaPrimitive{Set} is a very special type at the foundation of Agda's \vocab{type-system}, which we'll investigate in a little more depth in later notes.

      In these notes, we use \AgdaPrimitive{Set} for two seemingly-different purposes. Try not to think too hard about it for now, and just focus on the intuition for what each use of \AgdaPrimitive{Set} means.

      In these definitions, we write \AgdaPostulate{Prp} \AgdaSymbol{:} \AgdaPrimitive{Set} and \AgdaPostulate{Ctx} \AgdaSymbol{:} \AgdaPrimitive{Set} to define \AgdaPostulate{Prp} and \AgdaPostulate{Ctx} as new \vocabs{type}.

      \LongAgdaSnippet{types}

    \subsubsection{Judgements}

      The other use of \AgdaPrimitive{Set} is for our type of \vocabs{judgement}. Specifically, we'll use \AgdaPostulate{Prp} for \vocabs{proposition}, \AgdaPostulate{Ctx} for \vocabs{context}, and \AgdaPrimitive{Set} for \vocabs{judgement}. (\AgdaPostulate{Prp} and \AgdaPostulate{Ctx} are not judgements, though).

      These definitions are still within the \AgdaKeyword{postulate} block. In these definitions, each \vocab{type} to the left of an arrow is the type of the corresponding operator \vocab{position}, and the use of \AgdaPrimitive{Set} at the end of each type indicates that each of these operators is used to create a judgement.

      \LongAgdaSnippet{judgements}

      More generally, we almost always use the \AgdaPrimitive{Set} type in Agda to represent whatever sort of \vocabs{expression} we have in the \vocabs{premise} and \vocabs{conclusion} of our proof trees. In previous Agda notes we used \AgdaPrimitive{Set} as our type of propositions, to represent proof trees with propositions in the premises and conclusions. In these notes we're representing proof trees that contain judgements instead of propositions, so we use \AgdaPrimitive{Set} as our type of judgements.

    \subsubsection{Propositions}

      We use an Agda shorthand here to define three operators with the same type in a single definition. This has the same meaning as if we gave a separate definition for each operator on its own line.

      \LongAgdaSnippet{props}

    \subsubsection{Contexts}

      The context operators are defined in a similar way, but notice the type of the $\position{\cons}\position$ operator: it guarantees that the left side must be a proposition and the right side must be a context. Agda will give us a type error if we use the operator incorrectly.

      \LongAgdaSnippet{contexts}

    \subsubsection{Rules}

      The definitions of the inference rules for membership and subsumption judgements look very similar to the Agda inference rule definitions from previous notes. The major difference is that we're specifying the type of each \vocab{metavariable} explicitly.

      Remember that the arrows here are like ``separators'': each judgement to the left of an arrow is a \vocab{premise}, and the judgement at the end of each type is the \vocab{conclusion}.

      \LongAgdaSnippet{contextops}

      Previously, we only had one type of metavariable (propositions), so we could use \AgdaPrimitive{Set} for all of them. Now that we have contexts, Agda can help us to carefully distinguish between contexts, propositions, and judgements.

      We define the rules of \acrshort{ipl} with the same notation.

      \LongAgdaSnippet{ipl}

      Finally, at the end of the indented \AgdaKeyword{postulate} block, we define a couple \vocabs{derived-rule} that we can't build for ourselves in Agda quite yet. To prove these would require a more data-driven approach to representing our proof trees in Agda, which we'll see in later notes.

      \LongAgdaSnippet{derived}

    \subsubsection{Shorthand}

      The next few definitions in the code use more advanced features of Agda that we haven't introduced yet, in order to actually prove our use of the \inAt\ and \subAt\ shorthand derived rules.

      You're not expected to understand how this part of the code yet! We'll formally introduce \vocabs{natural-number} later, and we'll spend lots of time discussing how they work.

      For now, this is what to know in order to use the shorthand notation defined in this file:

      \paragraph{Natural numbers.} You can write natural numbers (non-negative integers including zero) the way you would in any programming language, like \AgdaNumber{0}, \AgdaNumber{5}, or \AgdaNumber{1234}.

      \paragraph{Lists of numbers.} You can write a list of natural numbers with the \AgdaInductiveConstructor{\_,\_} and \AgdaInductiveConstructor{[]} operators, with notation similar to a context of assumptions: \AgdaInductiveConstructor{[]} is the empty list and \AgdaNumber{1} \AgdaInductiveConstructor{,} \AgdaNumber{2} \AgdaInductiveConstructor{,} \AgdaNumber{3} \AgdaInductiveConstructor{,} \AgdaInductiveConstructor{[]} is a three-element list. Note that \insist{commas must be surrounded with spaces on both sides}, because the comma operator is just a regular operator. (This is a common annoyance in Adga, but it's the tradeoff for having very flexible operator notation.)

      \paragraph{The \AgdaFunction{$\in$-At} rule.} You can use \AgdaFunction{$\in$-At} with a natural number, like \AgdaFunction{$\in$-At} \AgdaNumber{0} or \AgdaFunction{$\in$-At} \AgdaNumber{12}. There must be a space between \AgdaFunction{$\in$-At} and the number. If \AgdaFunction{$\in$-At} is used as a branch in a larger proof tree, there must be parentheses around it, like \AgdaPostulate{Assumed} \AgdaSymbol{(}\AgdaFunction{$\in$-At} \AgdaNumber{5}\AgdaSymbol{)}.

      \paragraph{The \AgdaFunction{$\subsume$-At} rule.} You can use \AgdaFunction{$\subsume$-At} with a list of numbers, like \AgdaFunction{$\subsume$-At} \AgdaInductiveConstructor{[]} or \AgdaFunction{$\subsume$-At} \AgdaSymbol{(}\AgdaNumber{1} \AgdaInductiveConstructor{,} \AgdaInductiveConstructor{[]}\AgdaSymbol{)} or \AgdaFunction{$\subsume$-At} \AgdaSymbol{(}\AgdaNumber{1} \AgdaInductiveConstructor{,} \AgdaNumber{2} \AgdaInductiveConstructor{,} \AgdaNumber{3} \AgdaInductiveConstructor{,} \AgdaInductiveConstructor{[]}\AgdaSymbol{)}. If the list is nonempty, there must be parentheses around it. If \AgdaFunction{$\subsume$-At} is used as a branch in a larger proof tree, there must be parentheses around it too, like \mbox{\AgdaPostulate{Rearrange} \AgdaSymbol{(}\AgdaFunction{$\subsume$-At} \AgdaInductiveConstructor{[]}\AgdaSymbol{)}} \AgdaPostulate{$\top$-Intro} or \AgdaPostulate{Rearrange} \AgdaSymbol{(}\AgdaFunction{$\subsume$-At} \AgdaSymbol{(}\AgdaNumber{1} \AgdaInductiveConstructor{,} \AgdaNumber{2} \AgdaInductiveConstructor{,} \AgdaNumber{3} \AgdaInductiveConstructor{,} \AgdaInductiveConstructor{[]}\AgdaSymbol{))} \AgdaPostulate{$\top$-Intro}.

      In general, the \AgdaFunction{$\in$-At} and \AgdaFunction{$\subsume$-At} definitions in Agda work the same way as the \inAt\ and \subAt\ rules on paper. They invoke Agda's \vocab{proof-search} algorithm, so they may fail on very large contexts: proof search is an \vocab{undecidable} problem in general, so Agda's proof search algorithm is programmed to just ``give up'' after a certain amount of searching. It's rare to have a very large context, though, and proof search will work fine for the relatively small contexts that we'll be considering.

    \subsubsection{Arbitrary constants}

      Like before, we use the \AgdaKeyword{variable} keyword to define a set of \vocabs{arbitrary-constant} to reason with. This time we use the \AgdaPostulate{Prp} type instead of the \AgdaPostulate{Set} type, to explicitly indicate that our arbitrary constants are \vocabs{proposition}.

      \LongAgdaSnippet{arbitraries}



\section{Proofs}


  Let's prove some real properties of \acrshort{ipl}! Now that we have a full set of introduction and elimination rules for each operator, we can prove several fundamental properties about our use of the operators.

  This section will certainly not cover all of the things we can prove in \acrshort{ipl} at this point, but these are properties that we would probably intuitively expect to hold true, so proving them gives us more confidence that our proof system has the meaning that we expect it to have.


  \subsection{Commutativity}

    In general, we can ``swap'' the propositions in a \vocab{conjunction} ($\wedge$) or \vocab{disjunction} ($\vee$): both operators are \vocab{commutative}. Note that the \vocab{implication} operator ($\subset$) is \insist{not} commutative!

    \subsubsection{Conjunction}

      Here is a proof that the \vocab{conjunction} operator is \vocab{commutative} in \acrshort{ipl}.

      \longexample{
        \begin{prooftree}
                    \AxiomC{}
                  \LeftLabel{\inAt{0}}
                  \UnaryInfC{$\mono{P} \wedge \mono{Q} \in \mono{P} \wedge \mono{Q} \cons \emptyset$}
                \LeftLabel{\assumed}
                \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P} \wedge \mono{Q}$}
              \LeftLabel{\andElimR}
              \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{Q}$}
                    \AxiomC{}
                  \RightLabel{\inAt{0}}
                  \UnaryInfC{$\mono{P} \wedge \mono{Q} \in \mono{P} \wedge \mono{Q} \cons \emptyset$}
                \RightLabel{\assumed}
                \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P} \wedge \mono{Q}$}
              \RightLabel{\andElimL}
              \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P}$}
            \LeftLabel{\andIntro}
            \BinaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{Q} \wedge \mono{P}$}
          \LeftLabel{\implIntro}
          \UnaryInfC{$\emptyset \entails \left(\mono{P} \wedge \mono{Q}\right) \subset \left(\mono{Q} \wedge \mono{P}\right)$}
        \end{prooftree}
      }
      \LongAgdaSnippet{and-commutative}

      Why does proving $\left(\mono{P} \wedge \mono{Q}\right) \subset \left(\mono{Q} \wedge \mono{P}\right)$ mean that $\wedge$ is commutative? The answer is in how this proposition interacts with the \implElim\ rule.

      Remember that we read the $\subset$ operator as an ``if\ldots then\ldots'' \vocab{conditional} statement. By proving $\left(\mono{P} \wedge \mono{Q}\right) \subset \left(\mono{Q} \wedge \mono{P}\right)$, we are showing that \insist{if} we have a proof of $\mono{P} \wedge \mono{Q}$, \insist{then} we can prove $\mono{Q} \wedge \mono{P}$. This is exactly how our proof of commutativity interacts with the \implElim\ rule: if we can supply a proof of $\emptyset \entails \mono{P} \wedge \mono{Q}$, we get a proof of $\emptyset \entails \mono{Q} \wedge \mono{P}$. 

      \longexample{
        \begin{prooftree}
                      \AxiomC{}
                    \LeftLabel{\inAt{0}}
                    \UnaryInfC{$\mono{P} \wedge \mono{Q} \in \mono{P} \wedge \mono{Q} \cons \emptyset$}
                  \LeftLabel{\assumed}
                  \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P} \wedge \mono{Q}$}
                \LeftLabel{\andElimR}
                \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{Q}$}
                      \AxiomC{}
                    \RightLabel{\inAt{0}}
                    \UnaryInfC{$\mono{P} \wedge \mono{Q} \in \mono{P} \wedge \mono{Q} \cons \emptyset$}
                  \RightLabel{\assumed}
                  \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P} \wedge \mono{Q}$}
                \RightLabel{\andElimL}
                \UnaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{P}$}
              \LeftLabel{\andIntro}
              \BinaryInfC{$\mono{P} \wedge \mono{Q} \cons \emptyset \entails \mono{Q} \wedge \mono{P}$}
            \LeftLabel{\implIntro}
            \UnaryInfC{$\emptyset \entails \left(\mono{P} \wedge \mono{Q}\right) \subset \left(\mono{Q} \wedge \mono{P}\right)$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRule$}
            \UnaryInfC{$\emptyset \entails \mono{P} \wedge \mono{Q}$}
          \BinaryInfC{$\emptyset \entails \mono{Q} \wedge \mono{P}$}
        \end{prooftree}
      }

      Since we chose the \vocabs{arbitrary-constant} \mono{P} and \mono{Q} as our propositions, this same proof tree structure can be used two ``swap'' \insist{any} two propositions joined by the $\wedge$ operator.

      Reading the proof of $\emptyset \entails \left(\mono{P} \wedge \mono{Q}\right) \subset \left(\mono{Q} \wedge \mono{P}\right)$ bottom-up, we might write out the \implIntro\ step as a command to the reader, saying ``assume $\mono{P} \wedge \mono{Q}$'':

      \begin{enumerate}
        \item We want to show that the $\wedge$ operator is commutative. For any two arbitrary propositions \mono{P} and \mono{Q}, $\mono{P} \wedge \mono{Q}$ should imply $\mono{Q} \wedge \mono{P}$.
        \item Assume $\mono{P} \wedge \mono{Q}$.
        \item By the definition of $\wedge$, to prove $\mono{Q} \wedge \mono{P}$, we need to prove \mono{Q} and prove \mono{P}.
        \item To prove $\mono{Q}$ (top-down):
          \begin{enumerate}
            \item We have $\mono{P} \wedge \mono{Q}$ by assumption. 
            \item By the definition of $\wedge$, this means we have both a proof of \mono{P} and a proof of \mono{Q}, so we have a proof of \mono{Q}.
          \end{enumerate}
        \item To prove $\mono{P}$ (top-down):
          \begin{enumerate}
            \item We have $\mono{P} \wedge \mono{Q}$ by assumption. 
            \item By the definition of $\wedge$, this means we have both a proof of \mono{P} and a proof of \mono{Q}, so we have a proof of \mono{P}.
          \end{enumerate}
      \end{enumerate}

    \subsubsection{Disjunction}

      The proof of commutativity for \vocab{disjunction} is fairly different than the proof for \vocab{conjunction}: they have very different meanings even though they have some similar algebraic properties!

      \longexample{
        $\mono{C} \defequals \mono{P} \vee \mono{Q} \cons \emptyset$

        \begin{prooftree}
                  \AxiomC{}
                \LeftLabel{\inAt{0}}
                \UnaryInfC{$\mono{P} \vee \mono{Q} \in \mono{C}$}
              \LeftLabel{\assumed}
              \UnaryInfC{$\mono{C} \entails \mono{P} \vee \mono{Q}$}
                    \AxiomC{}
                  \RightLabel{\inAt{0}}
                  \UnaryInfC{$\mono{P} \in \mono{P} \cons \mono{C}$}
                \RightLabel{\assumed}
                \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{P}$}
              \RightLabel{\orIntroR}
              \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{Q} \vee \mono{P}$}
                    \AxiomC{}
                  \RightLabel{\inAt{0}}
                  \UnaryInfC{$\mono{Q} \in \mono{Q} \cons \mono{C}$}
                \RightLabel{\assumed}
                \UnaryInfC{$\mono{Q} \cons \mono{C} \entails \mono{Q}$}
              \RightLabel{\orIntroL}
              \UnaryInfC{$\mono{Q} \cons \mono{C} \entails \mono{Q} \vee \mono{P}$}
            \LeftLabel{\orElim}
            \TrinaryInfC{$\mono{C} \entails \mono{Q} \vee \mono{P}$}
          \LeftLabel{\implIntro}
          \UnaryInfC{$\emptyset \entails \left(\mono{P} \vee \mono{Q}\right) \subset \left(\mono{Q} \vee \mono{P}\right)$}
        \end{prooftree}
      }

      \LongAgdaSnippet{or-commutative}

      \begin{enumerate}
        \item We want to show that the $\wedge$ operator is commutative. For any two arbitrary propositions \mono{P} and \mono{Q}, $\mono{P} \vee \mono{Q}$ should imply $\mono{Q} \vee \mono{P}$.
        \item Assume $\mono{P} \vee \mono{Q}$. Now we need to prove $\mono{Q} \vee \mono{P}$.
        \item We have $\mono{P} \vee \mono{Q}$ by assumption. By the definition of $\vee$, this means we either have a proof of \mono{P} or a proof of \mono{Q}.
        \item If we have a proof of \mono{P}:
          \begin{enumerate}
            \item By the definition of $\vee$, we can use our proof of \mono{P} to prove $\mono{Q} \vee \mono{P}$.
          \end{enumerate}
        \item If we have a proof of \mono{Q}:
          \begin{enumerate}
            \item By the definition of $\vee$, we can use our proof of \mono{Q} to prove $\mono{Q} \vee \mono{P}$.
          \end{enumerate}
      \end{enumerate}


  \subsection{Associativity}

    In general, we can ``rearrange'' the parentheses around multiple \vocabs{conjunction} or multiple \vocabs{disjunction}: both operators are \vocab{associative}. Note that the \vocab{implication} operator is not associative!

    To prove associativity, we generally have to prove \insist{two} judgements showing that we can convert ``right-leaning'' expressions into ``left-leaning'' expressions \insist{and} vice versa. The two proofs are generally ``mirror images'' of each other, so we'll only give one side of each associativity proof in these notes.

    \subsubsection{Conjunction}

      The associativity proof for \vocab{conjunction} uses the \andElim\ rules to ``break down'' the assumption into three pieces, so that it can build them up with \andIntro\ in the necessary order.

      \longexample{
        $\mono{C} \defequals \mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right) \cons \emptyset$

        \begin{prooftree}
                      \AxiomC{}
                    \LeftLabel{\inAt{0}}
                    \UnaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right) \in \mono{C}$}
                  \LeftLabel{\assumed}
                  \UnaryInfC{$\mono{C} \entails \mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)$}
                \LeftLabel{\andElimL}
                \UnaryInfC{$\mono{C} \entails \mono{P}$}
                        \AxiomC{}
                      \LeftLabel{\inAt{0}}
                      \UnaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right) \in \mono{C}$}
                    \LeftLabel{\assumed}
                    \UnaryInfC{$\mono{C} \entails \mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)$}
                  \LeftLabel{\andElimR}
                  \UnaryInfC{$\mono{C} \entails \mono{Q} \wedge \mono{R}$}
                \LeftLabel{\andElimL}
                \UnaryInfC{$\mono{C} \entails \mono{Q}$}
              \LeftLabel{\andIntro}
              \BinaryInfC{$\mono{C} \entails \mono{P} \wedge \mono{Q}$}
                      \AxiomC{}
                    \RightLabel{\inAt{0}}
                    \UnaryInfC{$\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right) \in \mono{C}$}
                  \RightLabel{\assumed}
                  \UnaryInfC{$\mono{C} \entails \mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)$}
                \RightLabel{\andElimR}
                \UnaryInfC{$\mono{C} \entails \mono{Q} \wedge \mono{R}$}
              \RightLabel{\andElimR}
              \UnaryInfC{$\mono{C} \entails \mono{R}$}
            \LeftLabel{\andIntro}
            \BinaryInfC{$\mono{C} \entails \left(\mono{P} \wedge \mono{Q}\right) \wedge \mono{R}$}
          \LeftLabel{\implIntro}
          \UnaryInfC{$\emptyset \entails \left(\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)\right) \subset \left(\left(\mono{P} \wedge \mono{Q}\right) \wedge \mono{R}\right)$}
        \end{prooftree}
      }

      \LongAgdaSnippet{and-associative}

      Again, if we're being precise, this is only \insist{half} of the proof that conjunction is associative: remember that the $\subset$ operator is \insist{not} \vocab{commutative}, so it is possible for an operator to be associative in one direction and non-associative in the opposite direction.

      Specifically, the judgement we're missing a proof of is \mbox{$\emptyset \entails \left(\left(\mono{P} \wedge \mono{Q}\right) \wedge \mono{R}\right) \subset \left(\mono{P} \wedge \left(\mono{Q} \wedge \mono{R}\right)\right)$}. All we need to do is ``mirror'' the proof above: \andElimL\ becomes \andElimR\ and vice versa, and the nested \andIntro\ rules ``lean to the right'' instead of ``leaning to the left''.

    \subsubsection{Disjunction}

      Just like in the proof for conjunction, the associativity proof for \vocab{disjunction} also uses the \orElim\ rule to ``break down'' the assumption and uses the \orIntroL\ and \orIntroR\ rules to build them up in the necessary order. The \orElim\ rule behaves much differently than the \andElim\ rules, though, so the disjunction proof has a significantly different structure than the conjunction proof.

      Proof trees using \orElim\ get awfully wide, so the Agda code is printed on this page and the tree is printed sideways on the next page. If you're reading this on a monitor, most PDF reader applications have a way to rotate the page so you don't have to rotate your monitor or your head.

      Like with conjunction, we can ``mirror'' this proof by swapping \orIntroL\ with \orIntroR\ and having the nested \orElim\ rules ``lean to the left'' instead of ``leaning to the right''.

      \LongAgdaSnippet{or-associative}

      \clearpage

      \longexample{
        \begin{center}
          \rotatebox{270}{
            \begin{tabular}{l}
              $\mono{L} \defequals \left(\mono{P} \vee \mono{Q}\right) \vee \mono{R}$ \\
              $\mono{R} \defequals \mono{P} \vee \left(\mono{Q} \vee \mono{R}\right)$ \\
              $\mono{C} \defequals \mono{R} \cons \emptyset$ \\
              $\mono{D} \defequals \mono{Q} \vee \mono{R} \cons \mono{C}$ \\

                      \AxiomC{}
                    \LeftLabel{\inAt{0}}
                    \UnaryInfC{$\mono{R} \in \mono{C}$}
                  \LeftLabel{\assumed}
                  \UnaryInfC{$\mono{C} \entails \mono{R}$}
                          \AxiomC{}
                        \RightLabel{\inAt{0}}
                        \UnaryInfC{$\mono{P} \in \mono{P} \cons \mono{C}$}
                      \RightLabel{\assumed}
                      \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{P}$}
                    \RightLabel{\orIntroL}
                    \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{P} \vee \mono{Q}$}
                  \RightLabel{\orIntroL}
                  \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{L}$}
                        \AxiomC{}
                      \LeftLabel{\inAt{0}}
                      \UnaryInfC{$\mono{Q} \vee \mono{R} \in \mono{D}$}
                    \LeftLabel{\assumed}
                    \UnaryInfC{$\mono{D} \entails \mono{Q} \vee \mono{R}$}
                            \AxiomC{}
                          \RightLabel{\inAt{0}}
                          \UnaryInfC{$\mono{Q} \in \mono{Q} \cons \mono{D}$}
                        \RightLabel{\assumed}
                        \UnaryInfC{$\mono{Q} \cons \mono{D} \entails \mono{Q}$}
                      \RightLabel{\orIntroR}
                      \UnaryInfC{$\mono{Q} \cons \mono{D} \entails \mono{P} \vee \mono{Q}$}
                    \RightLabel{\orIntroL}
                    \UnaryInfC{$\mono{Q} \cons \mono{D} \entails \mono{L}$}
                          \AxiomC{}
                        \RightLabel{\inAt{0}}
                        \UnaryInfC{$\mono{R} \in \mono{R} \cons \mono{D}$}
                      \RightLabel{\assumed}
                      \UnaryInfC{$\mono{R} \cons \mono{D} \entails \mono{R}$}
                    \RightLabel{\orIntroR}
                    \UnaryInfC{$\mono{R} \cons \mono{D} \entails \mono{L}$}
                  \RightLabel{\orElim}
                  \TrinaryInfC{$\mono{Q} \vee \mono{R} \cons \mono{C} \entails \mono{L}$}
                \LeftLabel{\orElim}
                \TrinaryInfC{$\mono{C} \entails \mono{L}$}
              \LeftLabel{\implIntro}
              \UnaryInfC{$\emptyset \entails \mono{R} \subset \mono{L}$}
              \DisplayProof
            \end{tabular}
          }
        \end{center}
      }


  \subsection{Currying}

    Remember, the $\subset$ operator is neither \vocab{commutative} nor \vocab{associative}. What properties does it have?

    One property that we can prove about $\subset$ is commonly known as the principle of \vocab{currying}, especially in the context of modern programming language theory. Currying is named after Haskell Curry, the same logician who the Haskell programming language is named after.

    In informal language, the principle of currying says that the conditional ``if P and Q then R'' has the same meaning as the \insist{nested} conditional ``if P then `if Q then R'\thinspace''. Formally, this means that $\left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R}$ and $\mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)$ are \vocab{equivalent}: they both imply each other.

    Note that $\left(\mono{P} \subset \mono{Q}\right) \subset \mono{R}$ and $\mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)$ are \insist{not} equivalent! We'll see exactly why not when we introduce \vocab{lambda-calculus}, but this will be important to keep in mind.

    Here's one part of the proof. Traditionally, this direction is called \vocab{currying} and the other direction is called \vocab{uncurrying}.

    \longexample{
      $\mono{C} \defequals \left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R} \cons \emptyset$

      \begin{prooftree}
                    \AxiomC{}
                  \LeftLabel{\inAt{2}}
                  \UnaryInfC{$\left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R} \in \mono{Q} \cons \mono{P} \cons \mono{C}$}
                \LeftLabel{\assumed}
                \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{C} \entails \left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R}$}
                      \AxiomC{}
                    \LeftLabel{\inAt{1}}
                    \UnaryInfC{$\mono{P} \in \mono{Q} \cons \mono{P} \cons \mono{C}$}
                  \LeftLabel{\assumed}
                  \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{C} \entails \mono{P}$}
                      \AxiomC{}
                    \RightLabel{\inAt{0}}
                    \UnaryInfC{$\mono{Q} \in \mono{Q} \cons \mono{P} \cons \mono{C}$}
                  \RightLabel{\assumed}
                  \UnaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{C} \entails \mono{Q}$}
                \RightLabel{\andIntro}
                \BinaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{C} \entails \mono{P} \wedge \mono{Q}$}
              \LeftLabel{\implElim}
              \BinaryInfC{$\mono{Q} \cons \mono{P} \cons \mono{C} \entails \mono{R}$}
            \LeftLabel{\implIntro}
            \UnaryInfC{$\mono{P} \cons \mono{C} \entails \mono{Q} \subset \mono{R}$}
          \LeftLabel{\implIntro}
          \UnaryInfC{$\mono{C} \entails \mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)$}
        \LeftLabel{\implIntro}
        \UnaryInfC{$\emptyset \entails \left(\left(\mono{P} \wedge \mono{Q}\right) \subset \mono{R}\right) \subset \left(\mono{P} \subset \left(\mono{Q} \subset \mono{R}\right)\right)$}
      \end{prooftree}
    }

    \LongAgdaSnippet{curry}

    This is the first proof we've seen in the notes that uses \implIntro\ more than once, so pay close attention to the contexts in this proof.

    \longdemo{For the \vocab{uncurrying} part of the proof, we need to use \implIntro\ twice and then the \andElim\ rules to access each part of the assumption.}



\section{Heyting algebras}

  In general, the operators of \acrshort{ipl} form a \vocab{Heyting-algebra}. Technically, this will only be completely true once we add the $\bot$ operator in the notes on \vocab{negation}, but we can already prove nearly all of the Heyting algebra laws.

  This is a deep fact with many different aspects: Heyting algebras are relevant to many different areas of math and logic. For our purposes right now, the Heyting algebra laws give us many different example propositions that we should be able to prove.

  Here is a presentation of the laws of Heyting algebra written in our notation of \vocabs{entailment-judgement}. There are several equivalent ways to define the laws of Heyting algebra, so we will include some ``redundant'' laws here. We can prove all of these judgements in the system we have now.

  Don't worry about memorizing the names of all these properties: many of them come from the field of \vocab{abstract-algebra}, which isn't really what we're here to study. Just focus on trying to understand why each of these is valid based on our definitions.

  \begin{itemize}
    \item $\subset$ is reflexive: $\emptyset \entails \mono{P} \subset \mono{P}$
    \item $\subset$ is transitive: $\emptyset \entails \left(\left(\mono{P} \subset \mono{Q}\right) \wedge \left(\mono{Q} \subset \mono{R}\right)\right) \subset \left(\mono{P} \subset \mono{R}\right)$
    \item $\top$ is the maximum element of a bounded lattice: $\emptyset \entails \mono{P} \subset \top$
    \item $\vee$ is the join operator of a lattice: $\emptyset \entails \left(\mono{P} \subset \left(\mono{P} \vee \mono{Q}\right)\right) \wedge \left(\mono{Q} \subset \left(\mono{P} \vee \mono{Q}\right)\right)$
    \item $\wedge$ is the meet operator of a lattice: $\emptyset \entails \left(\left(\mono{P} \vee \mono{Q}\right) \subset \mono{P}\right) \wedge \left(\left(\mono{P} \vee \mono{Q}\right) \subset \mono{Q}\right)$
    \item $\vee$ is monotone with respect to $\subset$: $\emptyset \entails \left(\left(\mono{P} \subset \mono{Q}\right) \wedge \left(\mono{R} \subset \mono{S}\right)\right) \subset \left(\left(\mono{P} \vee \mono{R}\right) \subset \left(\mono{Q} \vee \mono{S}\right)\right)$
    \item $\wedge$ is monotone with respect to $\subset$: $\emptyset \entails \left(\left(\mono{P} \subset \mono{Q}\right) \wedge \left(\mono{R} \subset \mono{S}\right)\right) \subset \left(\left(\mono{P} \wedge \mono{R}\right) \subset \left(\mono{Q} \wedge \mono{S}\right)\right)$
    \item $\vee$ is idempotent:
      \begin{tabular}{l}
        $\emptyset \entails \mono{P} \subset \left(\mono{P} \vee \mono{P}\right)$ \\
        $\emptyset \entails \left(\mono{P} \vee \mono{P}\right) \subset \mono{P}$
      \end{tabular}
    \item $\wedge$ is idempotent:
      \begin{tabular}{l}
        $\emptyset \entails \mono{P} \subset \left(\mono{P} \wedge \mono{P}\right)$ \\
        $\emptyset \entails \left(\mono{P} \wedge \mono{P}\right) \subset \mono{P}$
      \end{tabular}
    \item $\vee$ and $\wedge$ are absorptive:
      \begin{tabular}{l}
        $\emptyset \entails \mono{P} \subset \left(\mono{P} \vee \left(\mono{P} \wedge \mono{Q}\right)\right)$ \\
        $\emptyset \entails \mono{P} \subset \left(\mono{P} \wedge \left(\mono{P} \vee \mono{Q}\right)\right)$ \\
        $\emptyset \entails \left(\mono{P} \vee \left(\mono{P} \wedge \mono{Q}\right)\right) \subset \mono{P}$ \\
        $\emptyset \entails \left(\mono{P} \wedge \left(\mono{P} \vee \mono{Q}\right)\right) \subset \mono{P}$
      \end{tabular}
  \end{itemize}

  These are just some of the things we can prove with \acrshort{ipl} at this point. Explore and see what else you can prove!



\input{include/footer.tex}
\end{document}
