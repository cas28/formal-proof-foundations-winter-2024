{-# OPTIONS --type-in-type #-}

open import Function using (case_of_)

data ∃ : (A : Set) → (A → Set) → Set where
  _,_ : ∀ {A : Set} {P : A → Set} (a : A) → P a → ∃ A P

witness : {A : Set} {P : A → Set} → ∃ A P → A
witness (a , p) = a

proof : {A : Set} {P : A → Set} (ex : ∃ A P) → P (witness ex)
proof (a , p) = p

-- Girard's paradox is a threat when Set : Set
here's-a-set : Set
here's-a-set = Set

-- universe polymorphism

-- Set : Set₁
-- Set₁ : Set₂

-- constructors never do anything nontrivial

-- \to   →

-- \top
data ⊤ : Set where
  tt : ⊤

-- \bot
data ⊥ : Set where

data Bool : Set where
  true : Bool
  false : Bool

-- \neg
¬_ : Set → Set
¬ A = A → ⊥

-- \and
data _∧_ : Set → Set → Set where
  _,_ : ∀ {A B} → A → B → A ∧ B

-- \or
data _∨_ : Set → Set → Set where
  left : ∀ {A B} → A → A ∨ B
  right : ∀ {A B} → B → A ∨ B

thing : ⊥ → ⊥
thing x = x

thing₂ : ∀ {P} → ¬ (P ∧ (¬ P))
thing₂ (p , np) = np p

explosion : ∀ {P : Set} → ⊥ → P
explosion ()

thing₃ : ∀ {A} → ⊥ → A ∨ (¬ A)
thing₃ b = explosion b

valueOfTypeTop : ⊤
valueOfTypeTop = tt

proof₁ : ⊤ ∧ ⊤
proof₁ = tt , tt

proof₂ : ⊤ ∨ ⊤
proof₂ = left tt

proof₃ : ⊤ ∨ ⊤
proof₃ = right tt

proof₄ : ⊤ → ⊤
proof₄ = λ x → x    -- \lambda
-- proof₄ x = x

∧-commutative : ∀ {A B} → (A ∧ B) → (B ∧ A)
∧-commutative (x , y) = y , x

∨-commutative : ∀ {A B} → (A ∨ B) → (B ∨ A)
∨-commutative (left x) = right x
∨-commutative (right y) = left y

∧-associative : ∀ {A B C} → A ∧ (B ∧ C) → (A ∧ B) ∧ C
∧-associative (x , (y , z)) = (x , y) , z

∧-associative′ : ∀ {A B C} → (A ∧ B) ∧ C → A ∧ (B ∧ C)
∧-associative′ ((x , y) , z) = x , (y , z)

∨-associative : ∀ {A B C} → A ∨ (B ∨ C) → (A ∨ B) ∨ C
∨-associative (left x) = left (left x)
∨-associative (right (left x)) = left (right x)
∨-associative (right (right x)) = right x

→-identity : {A : Set} → A → A
→-identity x = x

→-const : {A B : Set} → A → (B → A)
--       : {A B : Set} → A →  B → A
→-const x _ = x

→-apply : {A B : Set} → (A → B) → A → B
→-apply f x = f x

→-∧-dist : {A B C : Set} → (A → B ∧ C) → (A → B) ∧ (A → C)
→-∧-dist f =
  (λ x → case f x of λ { (b , _) → b })
  ,
  (λ x → case f x of λ { (_ , c) → c })

∧-→-dist : {A B C : Set} → (A → B) ∧ (A → C) → (A → B ∧ C)
∧-→-dist (f , g) x = f x , g x

-- indeterminate : ∀ {A} → ¬ (¬ A) → A
-- indeterminate nna = explosion (nna (λ a → {!!}))

-- indeterminate : ∀ {A} → ¬ (¬ (¬ A) → A)
-- indeterminate x = {!!}

-- excludedMiddle : ∀ {A} → A ∨ (¬ A)
-- excludedMiddle = right (λ a → {!!})

example₅ : ({A : Set} → A ∨ (¬ A)) → {A : Set} → ¬ (¬ A) → A
example₅ exm {A} nna =
  case exm {A} of λ
    { (left a) → a
    ; (right na) → explosion (nna na)
    }

-- when A is classically true then ¬¬A is constructively true (provable)
-- when ¬¬A is constructively true (provable) then A is classially true

lemma₁₀₀ : ⊤ ∨ ⊥
lemma₁₀₀ = left tt

-- disjunction property
-- "if I have a proof of P ∨ Q, I can find out whether I have a proof
--  of P or a proof of Q"

-- classical setting:
-- define HasProof(P) := "I have a proof for P"
-- "if I have that HasProof(P ∨ Q) is true then I can find out whether
--  HasProof(P) is true or HasProof(Q) is true"

doubleNegIntro : ∀ {A} → A → ¬ (¬ A)
doubleNegIntro a na = na a

Stable : Set → Set
Stable A = ¬ (¬ A) → A

-- all negated propositions are stable
--               ∀ {A} → ¬ (¬ (¬ A)) → ¬ A
doubleNegLemma : ∀ {A} → Stable (¬ A)
doubleNegLemma nnna a = nnna (doubleNegIntro a)

Decidable : Set → Set
Decidable A = A ∨ (¬ A)

-- imagine I have a type ℕ
--         I have a predicate IsZero : ℕ → Set
--         I have a proof of IsZero 0
--         I have a disproof of IsZero (1 + n) for all n

-- my decision procedure would look like:
-- decideIsZero : ∀ (x : ℕ) → Decidable (IsZero x)

-- possible : ∀ {A} → Decidable A → Stable A
-- possible? : ∀ {A} → Stable A → Decidable A

-- injectivity: ∀ m n. (suc m = suc n) → (m = n)
-- generativity (partial): ∀ n. suc n ≠ n

-- \equiv
data _≡_ : ∀ {A : Set} → A → A → Set where
  refl : ∀ {A : Set} (x : A) → x ≡ x

-- \bN
-- start at 0
data ℕ : Set where -- Peano naturals
  zero : ℕ
  suc : ℕ → ℕ -- successor

{-# BUILTIN NATURAL ℕ #-}

suc-injective : ∀ {m n : ℕ} → suc m ≡ suc n → m ≡ n
suc-injective (refl (suc m)) = refl m

data ℤ : Set where
  neg : ℕ → ℤ
  pos : ℕ → ℤ
  -- neg0≡pos0 : neg zero ≡ pos zero

neg0≡pos0 : ¬ (neg zero ≡ pos zero)
neg0≡pos0 ()

data IsNeg : ℤ → Set where
  isNeg : ∀ (n : ℕ) → IsNeg (neg n)

-- \approx
data _≈_ : ℤ → ℤ → Set where
  refl : ∀ (x : ℤ) → x ≈ x
  neg0≈pos0 : neg zero ≈ pos zero
  pos0≈neg0 : pos zero ≈ neg zero

cong-suc : ∀ (x y : ℕ) → x ≡ y → suc x ≡ suc y
cong-suc x .x (refl .x) = refl (suc x)

one : ℕ
one = suc zero

two : ℕ
two = suc one

three : ℕ
three = suc (suc (suc zero))

-- identity ≜
-- definitional equality :=   =
-- propositional equality ≡

-- zero + x ≡ x       "for free"
-- x + zero ≡ x       not "for free"
_+_ : ℕ → ℕ → ℕ
zero + y = y
(suc x) + y = suc (x + y)

_*_ : ℕ → ℕ → ℕ
zero * y = zero
(suc x) * y = y + (x * y)

onePlusOneEqualsTwo : (one + one) ≡ two
onePlusOneEqualsTwo = refl (zero + two)

+-zeroL : ∀ (n : ℕ) → (zero + n) ≡ n
+-zeroL n = refl n

+-zeroR : ∀ (n : ℕ) → (n + zero) ≡ n
+-zeroR zero = refl zero
+-zeroR (suc n) = cong-suc _ _ (+-zeroR n)

-- threePlusZeroIsThree : (three + zero) ≡ three
-- threePlusZeroIsThree =
--   cong-suc _ _ (plusZeroR _
--     (cong-suc _ _ (plusZeroR _
--        (cong-suc _ _ (plusZeroR _
--          (refl zero))))))

*-zeroL : ∀ (n : ℕ) → (zero * n) ≡ zero
*-zeroL n = refl zero

*-zeroR : ∀ (n : ℕ) → (n * zero) ≡ zero
*-zeroR zero = refl zero
*-zeroR (suc n) = *-zeroR n

-- suc n * zero
-- zero + (n * zero)
-- n * zero

fiveUnNormalized : ℕ
fiveUnNormalized = two + three

fiveNormalized : ℕ
fiveNormalized = suc (suc (suc (suc (suc zero))))

-- two = suc (suc zero)
-- three = suc (suc (suc zero))
-- two + three

--   +       2 = suc - suc - zero
--  / \      3 = suc - suc - suc - zero
-- 2   3

-- suc (suc zero) + suc (suc (suc zero))
--   pattern result: x = suc zero   y = suc (suc (suc zero))
--   output: suc (suc zero + suc (suc (suc zero)))
--   output: suc (suc (suc (suc (suc zero))))

-- suc zero + suc (suc (suc zero))
--   pattern result: x = zero, y = suc (suc (suc zero))
--   output: suc (zero + suc (suc (suc zero)))
--   output: suc (suc (suc (suc zero)))

-- zero + suc (suc (suc zero))
--   pattern result: y = suc (suc (suc zero))
--   output: suc (suc (suc zero))

data Even : ℕ → Set where
  ezero : Even zero
  esuc : ∀ {n : ℕ} → Even n → Even (suc (suc n))

thereIsAnEvenNumber : ∃ ℕ Even
thereIsAnEvenNumber = 0 , ezero

evenTwo : Even (suc (suc zero))
evenTwo = esuc ezero

four : ℕ
four = suc (suc (suc (suc zero)))

fourEqualsFour : four ≡ four
fourEqualsFour = refl four

evenFour : Even four
evenFour = esuc (esuc ezero)

oneIsNotEven : ¬ (Even (suc zero))
oneIsNotEven ()

threeIsNotEven : ¬ (Even (suc (suc (suc zero))))
threeIsNotEven (esuc ())

fiveIsNotEven : ¬ (Even (suc (suc (suc (suc (suc zero))))))
fiveIsNotEven (esuc (esuc ()))

-- associativity: x + (y + z) ≡ (x + y) + z
-- commutativity: x + y ≡ y + x
-- symmetry: x ≡ y → y ≡ x
-- transitive: x ≡ y → y ≡ z → x ≡ z
-- congruence: ∀ f. x ≡ y → f x ≡ f y

≡-symmetric : ∀ {A : Set} {x y : A} → x ≡ y → y ≡ x
≡-symmetric (refl x) = refl x

sym = ≡-symmetric

≡-transitive : ∀ {A : Set} {x y z : A} → x ≡ y → y ≡ z → x ≡ z
≡-transitive (refl x) (refl x) = refl x

trans = ≡-transitive

≡-congruence : ∀
  {A B : Set} {x y : A} (f : A → B) →
  x ≡ y → f x ≡ f y
≡-congruence f (refl x) = refl (f x)

cong = ≡-congruence

-- axiom J
-- subst
≡-substitutive : ∀ {A B : Set} → A ≡ B → A → B
≡-substitutive (refl A) x = x

-- Leibniz equality: two things are equal if for any property P,
--                   either both have property P,
--                   or neither have property P
subst : ∀ {A : Set} (P : A → Set) {x y : A} → x ≡ y → P x → P y
subst P (refl A) x = x

-- uniqueness of identity proofs
-- uniqueness of equality proofs
axiomK : ∀ {A : Set} {x y : A} (p : x ≡ y) (q : x ≡ y) → p ≡ q
axiomK (refl x) (refl x) = refl (refl x)

axiomK-for-top : (p : ⊤) (q : ⊤) → p ≡ q
axiomK-for-top tt tt = refl tt

+-suc-lemma : ∀ (x y : ℕ) → ((suc x) + y) ≡ (x + (suc y))
+-suc-lemma zero y = refl (suc y)
+-suc-lemma (suc x) y = cong-suc _ _ (+-suc-lemma x y)

+-commutative : ∀ (x y : ℕ) → (x + y) ≡ (y + x)
+-commutative zero y = ≡-symmetric (+-zeroR y)
+-commutative (suc x) y = 
  ≡-transitive
    (cong-suc _ _ (+-commutative x y))
    (+-suc-lemma y x)
  
+-associative : ∀ (x y z : ℕ) → (x + (y + z)) ≡ ((x + y) + z)
+-associative zero y z = refl (y + z)
+-associative (suc x) y z = ≡-congruence suc (+-associative x y z)

-- { Even zero ≐ Even (suc zero) }

-- { Even (suc (suc [n])) ≐ Even (suc (suc (suc zero))) }
-- { suc (suc [n]) ≐ suc (suc (suc zero)) }
-- { suc [n] ≐ suc (suc zero) }
-- { [n] ≐ suc zero }
-- [n] ↦ suc zero

-- https://agda.github.io/agda-stdlib/
even-lemma :
  ∀ (n : ℕ) →
    (suc (n + suc (n + zero))) ≡ (suc (suc (two * n)))
even-lemma n = 
  ≡-transitive
    (≡-congruence
      (λ (x : ℕ) → suc (n + suc x))
      (+-zeroR n))
    (≡-transitive
      (≡-congruence suc
        (≡-symmetric
          (+-suc-lemma n n)))
      (≡-symmetric
        (≡-congruence
          (λ x → suc (suc (n + x)))
          (+-zeroR n))))

*-2-even : ∀ (n : ℕ) → Even (suc (suc zero) * n)
*-2-even zero = ezero
*-2-even (suc n) =
  ≡-substitutive
    (≡-symmetric (≡-congruence Even (even-lemma n)))
    (esuc (*-2-even n))

-- Even (suc (n + suc (n + zero))) ~~ Even (suc (suc n))
-- Even (1 + n + 1 + n + 0) ~~ Even (suc (suc n))
-- Even (1 + 1 + n + n + 0) ~~ Even (suc (suc n))
-- Even (1 + 1 + n + n) ~~ Even (suc (suc n))
-- Even (2 + n + n) ~~ Even (suc (suc n))
-- Even (2 + (n + n)) ~~ Even (suc (suc n))
-- Even (suc (suc zero) + (n + n)) ~~ Even (suc (suc n))
-- Even (suc (suc (n + n))) ~~ Even (suc (suc n))
-- suc (n + suc (n + zero)) ≡ suc (suc n)

-- { Even (suc (suc [m])) ≐ Even (suc (n + suc (n + zero))) }
-- { suc (suc [m]) ≐ suc (n + suc (n + zero)) }
-- { suc [m] ≐ [n] + suc ([n] + zero) }

-- is x definitionally equal to y?
-- yes: they are equal by definition
-- no: they are inequal by definition
-- maybe: my algorithm cannot decide the problem with what's
--        currently in context

data IsZero : ℕ → Set where
  izzero : IsZero zero

zeroIsZero : IsZero zero
zeroIsZero = izzero

oneIsNotZero : ¬ (IsZero (suc zero))
oneIsNotZero ()

onePlusAnythingIsNotZero : ∀ (n : ℕ) → ¬ (IsZero (suc n))
onePlusAnythingIsNotZero n ()

-- { IsZero zero ≐ IsZero (suc [n]) }

eq : ℕ → ℕ → Bool
eq zero zero = true
eq zero (suc n) = false
eq (suc m) zero = false
eq (suc m) (suc n) = eq m n

-- Dec
Decision : Set → Set
Decision A = A ∨ (¬ A)

decEq : ∀ (m n : ℕ) → Decision (m ≡ n)
decEq zero zero = left (refl zero)
decEq zero (suc n) = right (λ ())
decEq (suc m) zero = right (λ ())
decEq (suc m) (suc n) = 
  case decEq m n of λ
    { (left p) → left (≡-congruence suc p)
    ; (right np) → right (λ p → np (suc-injective p))
    }

infixr 5 _∷_ -- x ∷ y ∷ z  =  x ∷ (y ∷ z)
data List : Set → Set where
  [] : ∀ {A : Set} → List A
  _∷_ : ∀ {A : Set} → A → List A → List A
  -- \::

data Vec : Set → ℕ → Set where
  [] : ∀ {A : Set} → Vec A zero
  _∷_ : ∀ {A : Set} {n : ℕ} → A → Vec A n → Vec A (suc n)

list123 : List ℕ
list123 = one ∷ (two ∷ (three ∷ []))

vec123 : Vec ℕ three
vec123 = one ∷ two ∷ three ∷ []

_++_ : ∀ {A : Set} → List A → List A → List A
[] ++ ys = ys
(x ∷ xs) ++ ys = x ∷ (xs ++ ys)

_++V_ : ∀ {A : Set} {m n : ℕ} → Vec A m → Vec A n → Vec A (m + n)
[] ++V ys = ys
(x ∷ xs) ++V ys = x ∷ (xs ++V ys)

-- ++V-nilR :
--   ∀ {A : Set} {m : ℕ} (xs : Vec A m) →
--   (xs ++V []) ≡ subst (Vec A) (sym (+-zeroR m)) xs
-- ++V-nilR = {!!}

snoc : ∀ {A : Set} → List A → A → List A
snoc xs y = xs ++ (y ∷ [])

reverse : ∀ {A : Set} → List A → List A
reverse [] = []
reverse (x ∷ xs) = snoc (reverse xs) x

++-assoc : ∀
  {A : Set} →
  (xs ys zs : List A) →
  (xs ++ (ys ++ zs)) ≡ ((xs ++ ys) ++ zs)
++-assoc [] ys zs = refl (ys ++ zs)
++-assoc (x ∷ xs) ys zs = cong (λ ws → x ∷ ws) (++-assoc xs ys zs)

++-nilL : ∀ {A : Set} → (xs : List A) → ([] ++ xs) ≡ xs
++-nilL xs = refl xs

++-nilR : ∀ {A : Set} → (xs : List A) → (xs ++ []) ≡ xs
++-nilR [] = refl []
++-nilR (x ∷ xs) = cong (λ ws → x ∷ ws) (++-nilR xs)

-- natural induction:
--   - P(zero)                        zero
--   - ∀ n. P(n) → P(suc n)           suc

-- list induction:                    structural induction
--   - P([])
--   - ∀ (x : A) (xs : List A). P(xs) → P(x ∷ xs)

++-reverse : ∀
  {A : Set} →
  (xs ys : List A) →
  reverse (xs ++ ys) ≡ (reverse ys ++ reverse xs)
++-reverse [] ys = sym (++-nilR (reverse ys))
++-reverse (x ∷ xs) ys = 
  trans
    (cong (λ ws → snoc ws x) (++-reverse xs ys))
    (sym (++-assoc (reverse ys) (reverse xs) (x ∷ [])))

reverse-reverse : ∀
  {A : Set} (xs : List A) →
  reverse (reverse xs) ≡ xs
reverse-reverse [] = refl []
reverse-reverse (x ∷ xs) =
  trans
    (++-reverse (reverse xs) (x ∷ []))
    (cong (λ ws → x ∷ ws) (reverse-reverse xs))

-- ++-reverse xs (x ∷ []) :
--   reverse (xs ++ (x ∷ [])) ≡ reverse (x ∷ []) ++ xs
--   reverse (xs ++ (x ∷ [])) ≡ (x ∷ reverse []) ++ xs
--   reverse (xs ++ (x ∷ [])) ≡ (x ∷ []) ++ xs
--   reverse (xs ++ (x ∷ [])) ≡ x ∷ ([] ++ xs)
--   reverse (xs ++ (x ∷ [])) ≡ x ∷ xs
--   reverse (snoc xs x) ≡ x ∷ xs

length : ∀ {A : Set} → List A → ℕ
length [] = zero
length (_ ∷ xs) = suc (length xs)

-- reverse-length : ∀
--   {A : Set} (xs : List A) →
--   length xs ≡ length (reverse xs)
-- reverse-length = {!!}

-- reverse-reverse-length : ∀
--   {A : Set} (xs : List A) →
--   length xs ≡ length (reverse (reverse xs))
-- reverse-reverse-length xs = {!!}

zip : ∀ {A B : Set} → List A → List B → List (A ∧ B)
zip [] [] = []
zip [] (x ∷ ys) = []
zip (x ∷ xs) [] = []
zip (x ∷ xs) (y ∷ ys) = (x , y) ∷ zip xs ys

safeZip : ∀
  {A B : Set} (xs : List A) (ys : List B) →
  length xs ≡ length ys →
  List (A ∧ B)
safeZip [] [] (refl zero) = []
safeZip (x ∷ xs) (y ∷ ys) eq = safeZip xs ys (suc-injective eq)

vecZip : ∀
  {A B : Set} {n : ℕ} (xs : Vec A n) (ys : Vec B n) →
  Vec (A ∧ B) n
vecZip [] [] = []
vecZip (x ∷ xs) (y ∷ ys) = (x , y) ∷ vecZip xs ys

-- safeZip-lemma : ∀
--   {A B : Set} (xs : List A) (ys : List B) →
--   (eq : length xs ≡ length ys) →
--   length (safeZip xs ys eq) ≡ length xs
-- safeZip-lemma = {!!}

data BinaryTree : Set where
  leaf : ℕ → BinaryTree
  node : ℕ → BinaryTree → BinaryTree → BinaryTree

tree₁ : BinaryTree
tree₁ =
  node 1
    (node 2 (leaf 3) (leaf 4))
    (leaf 5)

swap : BinaryTree → BinaryTree
swap (leaf x) = leaf x
swap (node x l r) = node x r l

data DepthTree : Set → ℕ → Set where
  leaf : {A : Set} → A → DepthTree A zero
  node :
    {A : Set} {n : ℕ} →
    A → DepthTree A n → DepthTree A n →
    DepthTree A (suc n)

depthTree₁ : DepthTree ℕ 1
depthTree₁ =
  node 1
    (leaf 2)
    (leaf 3)

depthTree₂ : ∃ ℕ (DepthTree ℕ)
depthTree₂ =
  3 ,
    node 1
      (node 2
        (node 3 (leaf 4) (leaf 5))
        (node 6 (leaf 7) (leaf 8)))
      (node 9
        (node 10 (leaf 11) (leaf 12))
        (node 13 (leaf 14) (leaf 15)))

swapD : {A : Set} (t : ∃ ℕ (DepthTree A)) → DepthTree A (witness t)
swapD (n , leaf x) = leaf x
swapD (n , node x l r) = node x r l

-- _≤_ : (m n : ℕ) → Set
-- m ≤ n = ∃ ℕ (λ x → (m + x) ≡ n)

data _≤_ : ℕ → ℕ → Set where
  zero≤any : (n : ℕ) → zero ≤ n
  suc≤suc : {m n : ℕ} → m ≤ n → suc m ≤ suc n

lte₁ : 0 ≤ 5
lte₁ = zero≤any 5

lte₂ : 1 ≤ 5
lte₂ = suc≤suc (zero≤any 4)

lte₃ : 2 ≤ 5
lte₃ = suc≤suc (suc≤suc (zero≤any 3))

difference : {m n : ℕ} → m ≤ n → ℕ
difference (zero≤any x) = x
difference (suc≤suc lte) = difference lte

difference-lemma : {m n : ℕ} (lte : m ≤ n) → (m + difference lte) ≡ n
difference-lemma (zero≤any x) = refl x
difference-lemma (suc≤suc lte) = cong suc (difference-lemma lte)

mutual
  data BST : Set where
    leaf : ℕ → BST
    node :
      (n : ℕ) →
      (l r : BST) →
      AllNodes≤ n l → AllNodes≥ n r →
      BST

  data AllNodes≤ : ℕ → BST → Set where
    leaf : {m n : ℕ} → m ≤ n → AllNodes≤ n (leaf m)
    node :
      {m n : ℕ} {l r : BST} →
      m ≤ n → (anl : AllNodes≤ m l) → (anr : AllNodes≤ m r) →
      AllNodes≤ n (node m l r anl {!!})

  data AllNodes≥ : ℕ → BST → Set where
    leaf : {m n : ℕ} → n ≤ m → AllNodes≥ n (leaf m)
    node :
      {m n : ℕ} {l r : BST} →
      n ≤ m → (anl : AllNodes≥ m l) → (anr : AllNodes≥ m r) →
      AllNodes≥ n (node m l r {!!} anr)

{-
     3
    / \
   1   4
  / \
 0   100
-}

-- bst₁ : BST 3
-- bst₁ =
--   node 3
--     (suc≤suc (zero≤any 2)) (suc≤suc (suc≤suc (suc≤suc (zero≤any 1))))
--     (leaf 1)
--     (leaf 4)
-- 
-- bst₂ : BST 3
-- bst₂ =
--   node 3
--     (suc≤suc (zero≤any 2)) (suc≤suc (suc≤suc (suc≤suc (zero≤any 1))))
--     (node 1 (zero≤any 1) (suc≤suc (zero≤any 4)) (leaf 0) (leaf 5))
--     (leaf 4)
