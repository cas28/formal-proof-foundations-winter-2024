\input{include/header.tex}

\title{Syllabus}


\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Disclaimer}

  This is only the second time that this course is being offered, so please be patient with me as I figure out how to teach it!

  I wrote up several weeks of properly-formatted reading material during the first offering of this course, but I'll still be writing some of the course material during this quarter.


\section{Contact info}

  \begin{tabular}{l l}
    Email: & \href{mailto:cas28@pdx.edu}{cas28@pdx.edu} \\
    Office: & Fourth Avenue Building (FAB) 115D \\
    \href{https://fishbowl.zulip.cs.pdx.edu}{\linkstyle{Zulip}}: & \href{https://fishbowl.zulip.cs.pdx.edu/#narrow/pm-with/361-cas28}{cas28}
  \end{tabular}

  FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the \href{https://www.pdx.edu/buildings/sites/g/files/znldhr2301/files/2021-10/Fourth\%20Avenue\%20Building\%20Floorplans.pdf}{\linkstyle{building plan}} (where L120-00 is the fishbowl).

  If you've never been to FAB before, come in the main entrance on 4\textsuperscript{th} Avenue and follow the hallway to your left until you see the Computer Science department branding on your right: that's the fishbowl.


\section{Discussion forum}

  We will be using the \href{https://fishbowl.zulip.cs.pdx.edu}{\linkstyle{Zulip}} instance hosted by the CS department as a discussion forum in this course.

  If you're registered for the course, you should be able to access the \mono{\#fpf-winter-2024} channel on Zulip. Check your inbox for an invite if you didn't already have a Zulip account. If you can't access the channel, please email me and I'll add you to the channel manually.


\section{Course description}

  Don't let this description scare you! The official description of a course is like the abstract of a paper: it's not meant to be a sales pitch, it's meant to be concise and technical.

  \subsection{Summary}

    CS 410/510 Formal Proof Foundations explores the application of dependently-typed proof systems in the foundations of mathematics, using a dependently-typed programming language such as Agda, Idris, or Lean. General subjects covered are proof theory, discrete mathematics, and total functional programming; specific topics include natural deduction, structural induction, generalized algebraic data types, dependent pattern matching, and elementary set theory and number theory in a constructive setting. Additional topics may include formal languages and automata in a constructive setting, well-founded induction, coinduction, and elementary category theory.

  \subsection{Prerequisites}

    \begin{tabular}{l p{5in}}
      Required: & CS 251 or equivalent introduction to Boolean logic \\
      Recommended: & Intro-level experience with functional programming and proofs about programs, as discussed in courses like CS 311, CS 320/358, or CS 457
    \end{tabular}

  \subsection{Goals}

    Upon successful completion of this course, students will be able to:

    \begin{itemize}
      \item Interpret and construct proofs in a system of natural deduction for first-order and higher-order constructive logic.
      \item Interpret and construct proofs in a dependently-typed programming language with dependent pattern matching and structural induction.
      \item Define and prove properties of foundational concepts in constructive mathematics, including but not limited to: sets, relations, equality, functions, ordered pairs, disjoint unions, decision procedures, natural numbers, integers, lists, and setoids.
      \item Design, implement, and prove properties of simple recursive programs in a total functional programming language subject to a conservative termination checker.
    \end{itemize}

  \subsection{Resources}

    There are no required textbooks. Most of the course material will be provided on Canvas. We may refer to some parts of these freely-available resources:

    \begin{itemize}
      \item \href{https://plfa.github.io}{\linkstyle{Programming Language Foundations in Agda, Part 1: Logical Foundations}}
      \item \href{http://www.cse.chalmers.se/~ulfn/papers/afp08/tutorial.pdf}{\linkstyle{Dependently Typed Programming in Agda}}
    \end{itemize}


\section{Lecture}

  Lecture is every Monday and Wednesday, 4:40-6:30PM.

  Attendance is not graded, but you are expected to keep up with the weekly schedule of lectures.

  To attend in person, come to Engineering Building (EB) room 92.

  To attend remotely, join the Zoom session at \linkstyle{\url{https://pdx.zoom.us/j/81693750223}}. Please keep your microphone off when you're not speaking.

  I will periodically check the Zoom chat for questions during lecture, and you can use the Zoom "raise hand" button to get my attention if you'd like to ask a question with voice and/or video.

  Each lecture will be recorded, with videos available on Canvas within 48 hours of the live lecture.


\section{Office hours}

  Office hours are available one-on-one or for small groups every Tuesday, 4:30-5:30PM.

  To attend in person, come to my office in FAB room 115D.

  To attend remotely, join the Zoom session at \linkstyle{\url{https://pdx.zoom.us/j/84104689473}}.


\section{Assignments}

    We will have five assignments throughout the quarter; you will have at least one week (7 full days) to work on each assignment after it's released. The schedule will depend a bit on how fast I can put together the later assignments that aren't written yet.

    In most assignments, we'll practice reasoning about proofs by manipulating traditional proof tree diagrams. It will be easiest to do this with a pencil and paper (or a tablet drawing device), but if you know \LaTeX\ you can do some of it painfully with \LaTeX.

    Some assignments will also include short answer questions with written English responses. You can write these on paper or type them in any text editor.

    Some assignments may involve reasoning about proofs by manipulating proof terms in the Agda proof language. This will require an editor with a special Agda plugin. We'll go through the setup process in the first set of lecture notes that involve Agda.

  \subsection{Submission}

    Each assignment will have a Canvas page for submissions. These will remain open until the end of the course.

    If you have work on paper to submit, use a scanner or a camera to digitize it. You can scan documents in the Portland State library, and most phone cameras these days have a high enough resolution to capture writing on paper. Make sure to adjust the lighting and contrast so that all of your writing is legible.


\section{Notes}

  Instead of an assignment submission, you may choose to submit a set of \textbf{thorough, timestamped notes for three lecture recordings}. You can take this option for up to three assignments in total.

  Your notes should be \textbf{useful to another student taking this course who is reviewing the lecture recording}. You can choose any style and format that you think will be effective.

  As long as you put in an honest effort, your notes will be graded only on completion, not on quality. You must submit notes for three recordings to get full assignment credit; submitting notes for one or two recordings will earn 1/3 or 2/3 of full assignment credit.

  You're not required to share your notes with other students in order to get full assignment credit for them, but I encourage you to share your notes and work together if you're comfortable with it! Obviously your notes should not be an exact copy of another students' submitted notes, but I don't think I have to worry too much about plagiarism for this work.


\section{Grading}

  Each assignment is weighted equally. You may get partial credit for showing your work even if you don't fully complete a problem.

  Each assignment will have a ``soft due date'' of at least one week after the date it was assigned. Every soft due date is timed at 11:59PM (just before midnight).

  There is no grade penalty for submitting an assignment after the soft due date. The only \insist{hard} deadline for all submissions is \insist{Sunday of week 11} (the end of ``finals week''). I won't have time to grade anything submitted after that point.

  If you submit an assignment by its soft due date, you will get feedback and a grade within one week, and then you'll have one more week to resubmit your assignment \insist{once} for an updated grade. This doesn't apply to the final assignment, just because there won't be time.

  If you submit an assignment after its soft due date (and before the hard deadline at the end of the quarter), I guarantee that it will get graded for full credit before the end of the quarter---but I'll grade your submission whenever it's convenient for me, and you will not have a chance to resubmit an updated version of it. That's the deal!

  If you have a DRC accommodation for extended deadlines, that applies to the soft due dates. In that case, email me when you need an extension so that I know when to grade your submission.


\section{Change history}

  Any changes I make to the material for this course are tracked in a GitLab repository:\\
  \linkstyle{\url{https://gitlab.cecs.pdx.edu/cas28/2024-winter-fpf}}

  You're not expected to interact with this repository, but if you ever want to know whether something has been modified, you can find the answer here.

  In general, I might modify any course material before we cover it in lecture - if you read ahead, I might have done some edits to the course material by the time we cover the topic in lecture that you read about.

  After we cover something in lecture, I'll only modify the related material if there are any corrections I need to make.

  
\end{document}
